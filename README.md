# 3d_Pacman
![image](https://github.com/SarraGha/3d_Pacman/assets/77792332/a7f4c6f0-463d-4466-aad6-a3c5d0742c3b)

This is a simple Pacman game developed using Unity. The game features a Pacman character that can move through a maze, eat cherries and rewards, and score points.

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Controls](#controls)
- [Gameplay](#gameplay)
- [Contributing](#contributing)
- [License](#license)

## Features

- Pacman character movement.
- Eating cherries to score points.
- Collecting rewards for higher scores.
- Basic scoring system.
- Maze-based gameplay.

## Installation

1. Clone the repository to your local machine.
   ```bash
   git clone https://github.com/your-username/pacman-unity.git
   ```

2. Open the project in Unity.

## Usage

1. Open the project in Unity.
2. Play the game in the Unity Editor or build it for your desired platform.

## Controls

- Use the arrow keys or WASD to move the Pacman character.
- Collect cherries to increase your score.

## Gameplay

- Navigate the Pacman through the maze.
- Eat cherries and collect rewards to score points.
- Avoid collisions with maze walls.
- The game ends when you achieve a certain score or complete a level.

## Contributing

Contributions are welcome! If you find any issues or have suggestions, feel free to open an issue or create a pull request.

## License

This project is licensed under the [MIT License](LICENSE).
